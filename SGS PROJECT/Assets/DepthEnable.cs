﻿using UnityEngine;
using System.Collections;

public class DepthEnable : MonoBehaviour {

	// Use this for initialization
	void Start () {
		Camera cam = GetComponent<Camera> ();
		cam.depthTextureMode = DepthTextureMode.Depth;
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
