﻿using UnityEngine;
using System.Collections;

public class WaterSpawner : MonoBehaviour {
	public GameObject water;
	public Camera cam;
	// Use this for initialization
	void Start () {

		for (int x = 0; x < transform.localScale.x; x++) {
			for (int y = 0; y < transform.localScale.y; y++) {
				for (int z = 0; z < transform.localScale.z; z++) {
					Vector3 pos = transform.position;
					pos.x += x;
					pos.y += y;
					pos.z += z;
					GameObject sph = (GameObject)Instantiate(water, pos, Quaternion.identity);
					sph.GetComponentInChildren<BillboardScript>().cam = cam;
				}
			}
		}
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
