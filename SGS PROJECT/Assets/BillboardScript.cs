﻿using UnityEngine;
using System.Collections;

public class BillboardScript : MonoBehaviour {

	public Camera cam;
	
	void Update()
	{
		transform.LookAt(transform.position - cam.transform.rotation * Vector3.back,
		                 cam.transform.rotation * Vector3.up);
	}
}
