﻿Shader "Custom/WaterDepthShader" {
Properties {
        _MainTex ("_MainTex", 2D) = "white" {}
    }
SubShader {
Tags { "RenderType"="Opaque" }

Pass{
CGPROGRAM
#pragma vertex vert
#pragma fragment frag
#include "UnityCG.cginc"

uniform sampler2D _CameraDepthTexture;
sampler2D _MainTex;

struct v2f {
   float4 pos : SV_POSITION;
   float4 scrPos:TEXCOORD1;
   float4 uv : TEXCOORD0;
};

//Vertex Shader
v2f vert (appdata_base v){
   v2f o;
   o.pos = mul (UNITY_MATRIX_MVP, v.vertex);
   o.scrPos=ComputeScreenPos(o.pos);
   o.uv = float4( v.texcoord.xy, 0, 0);
   //for some reason, the y position of the depth texture comes out inverted
   //o.scrPos.y = 1 - o.scrPos.y;
   return o;
}

//Fragment Shader
half4 frag (v2f i) : COLOR{
   float depthValue = Linear01Depth (tex2Dproj(_CameraDepthTexture, UNITY_PROJ_COORD(i.scrPos)).r);
   //fixed4 mainDepth = tex2D(_MainTex, i.uv);
   half4 depth;

   //depth.r = i.pos.x / 100.0;
   //depth.g = i.pos.y / 100.0;
   //depth.b = i.pos.z / 100.0;
   depth.r = depthValue;
   depth.g = depthValue;
   depth.b = depthValue;

   depth.a = 1;
   return depth;
}
ENDCG
}
}
FallBack "Diffuse"
}