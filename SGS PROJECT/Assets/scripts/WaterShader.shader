﻿Shader "Custom/WaterShader" {
    Properties {
        _MainTex ("Base (RGB)", 2D) = "white" {}
    }
    SubShader {
        Pass {
        	ZTest Off
        	Blend One One
        	
            CGPROGRAM
            #pragma vertex vert_img
            #pragma fragment frag

            #include "UnityCG.cginc"
            
            uniform sampler2D _MainTex;

            fixed4 frag(v2f_img i) : SV_Target {
            	float x = tex2D(_MainTex, i.uv).x;
                return fixed4(1.0, 1.0, 1.0, x);
            }
            ENDCG
        }
    }
    
    Fallback "VertexLit"
}