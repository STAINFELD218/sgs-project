﻿Shader "Custom/FinalShader" {
	Properties {
		_WaterTex ("_WaterTex", 2D) = "white" { }
		_WaterDepthTex ("_WaterDepthTex", 2D) = "white" { }
		_MainColorTex ("_MainColorTex", 2D) = "white" { }
		_MainDepthTex ("_MainDepthTex", 2D) = "white" { }
	}
	SubShader {
		Pass {
			Fog { Mode Off }
			CGPROGRAM
			 
			#pragma vertex vert
			#pragma fragment frag
			#include "UnityCG.cginc"

			sampler2D _MainColorTex;
			sampler2D _MainDepthTex;
			sampler2D _WaterTex;
			sampler2D _WaterDepthTex;
			uniform sampler2D _CameraDepthTexture;

			struct appdata {
            	float4 vertex : POSITION;
            	float4 texcoord : TEXCOORD0;
        	};
        	
			struct v2f {
				float4 pos : SV_POSITION;
				float4 uv : TEXCOORD0;
				float4 scrPos:TEXCOORD1;
			};
			v2f vert(appdata v) {
				v2f o;
				o.pos = mul(UNITY_MATRIX_MVP, v.vertex);
				o.scrPos=ComputeScreenPos(o.pos);
				o.uv = float4( v.texcoord.xy, 0, 0);
				return o;
			}
  
			fixed4 frag(v2f i) : SV_Target {
			//return fixed4 (1.0, 0.0, 0.0, 1.0);
				fixed4 mainScene = tex2D(_MainColorTex, i.uv);
				fixed4 mainDepth = tex2D(_MainDepthTex, i.uv);
				fixed4 waterScene = tex2D(_WaterTex, i.uv);
				//float waterDepth = tex2D(_WaterDepthTex, i.uv);	
				float waterDepth = Linear01Depth (tex2Dproj(_CameraDepthTexture, UNITY_PROJ_COORD(i.scrPos)).r);
  
							//float finalDepth = 1.0;
				//if (waterScene.a > 0.5) {
				//	finalDepth = (1.0 - waterScene.a) * 0.01 + waterDepth.x;
				//}
				float finalDepth = waterDepth;
				
				//return fixed4(waterDepth.xxx, 1.0);
				
				if (finalDepth <= mainDepth.x) {
					float waterOpacity = waterScene.a;//(mainDepth.x - waterDepth) * 200.0 * waterScene.a;
					
					if (waterOpacity > 0.5) {
						//waterOpacity *= (mainDepth.x - waterDepth) * 50.0;
						//return fixed4(0.0, 0.5, 1.0, 1.0);
						waterOpacity *= 0.8;
						float threshold = 0.01;
						
						if (mainDepth.x - finalDepth < threshold) {
							waterOpacity *= (mainDepth.x - finalDepth) / threshold;
						}
						
						return fixed4(mainScene.rgb * (1.0 - waterOpacity) + fixed3(0.3, 0.7, 1.0) * waterOpacity, 1.0);
					} else {
						return fixed4(mainScene.rgb, 1.0);
					}
				} else {
					return fixed4(mainScene.rgb, 1.0);
				}
				//return fixed4(waterDepth.xxx, 1.0);
				//return fixed4(waterScene.rgb, 1.0);
			}

			ENDCG
		}
	}
}