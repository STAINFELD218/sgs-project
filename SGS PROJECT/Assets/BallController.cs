﻿using UnityEngine;
using System.Collections;

public class BallController : MonoBehaviour
{
	
	public float speed;
	private Rigidbody rb;
	public float JumpPower;
	public float JumpTime;
	public bool IsGrounded;

	void Start ()
	{
		rb = GetComponent<Rigidbody> ();
	}
	
	void OnCollisionStay (Collision collisionInfo)
	{
		IsGrounded = true;
	}
	
	void OnCollisionExit (Collision collisionInfo)
	{
		IsGrounded = false;
	}

	void Update ()
	{

           
		if (Input.GetButton ("Jump")) {
			JumpTime += 1 * Time.deltaTime;

			if (IsGrounded == true) {
				JumpPower += 1 * Time.deltaTime * 10;
			}
		}
            
		if (Input.GetButtonUp ("Jump") || JumpTime >= 1) { 
			JumpPower = 0;
			JumpTime = 0;
		}

		float moveHorizontal = Input.GetAxis ("Horizontal");
		float moveVertical = Input.GetAxis ("Vertical");
			
		Vector3 movement = new Vector3 (moveHorizontal, JumpPower, moveVertical);
			
		rb.AddForce ((movement * speed - rb.velocity));

		if (transform.position.y < -10) {
			rb.position = new Vector3(0, 3, 0);
			rb.velocity = new Vector3(0, 0, 0);
		}
	}

	void OnTriggerEnter (Collider other)
	{
		if (other.gameObject.CompareTag ("Cristal")) {
			other.gameObject.SetActive (false);
		}
	}


}