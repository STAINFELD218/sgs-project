﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class GameManager : MonoBehaviour {

	public Text t;
	public Toggle tg1;
	public Toggle tg2;
	public Toggle tg3;
	int score=0;
	float time= 30.0f;
	string pname;

	public Slider sl;
	public Scrollbar sc1;
	public Scrollbar sc2;

	public GameObject EnemyPrefab;
	const int ENEMY_NUM = 3;

	GameObject[] enemy = new GameObject[ENEMY_NUM];

	public Text inputtext;

	// Use this for initialization
	void Start () {

		score = 0;
		time = 20.0f;
		pname = "nanashi";

		for(int i=0;i < ENEMY_NUM;i++){
			enemy[i]= (GameObject) Instantiate (EnemyPrefab);
			Vector3 p= enemy[i].transform.position;
			p.x = (Random.Range (0,2)*2 -1 )*2;
			p.y = (Random.Range (0,2)*2 -1 )*2;
			enemy[i].transform.position = p;
		}

	}
	
	// Update is called once per frame
	void Update () {
		//time -= Time.deltaTime;
		//if (time >= 0.0f) {
		//	t.text = string.Format ("score:{0} time:{1:f1}", score, time);
		//} else {
		//	t.text = string.Format ("score:{0} finished!", score);
		//}

		//togle game
		time -= Time.deltaTime;
		if (time >= 0.0f){
			t.text = pname + string.Format ("'s score is{0}pts. time:{1:f1}", score, time);
		} else {
			t.text = pname + string.Format ("'s score is{0}pts. finished!", score);
		}
	
	}

	public void ToggleChanged(){
		if (tg1.isOn==false && tg2.isOn==false && tg3.isOn==false ) {
			if(time >= 0.0f){
				score++;
			}
			int r = Random.Range(0,3);
			if(r==0) tg1.isOn=true;
			if(r==1) tg2.isOn=true;
			if(r==2) tg3.isOn=true;
		}
	}

	public void SetColor(){
		t.color = new Color(sl.value, sc1.value, sc2.value);
	}

	public void SetName(){
		pname = inputtext.text;
		score = 0;
		time = 20.0f;
		tg1.isOn = true;
		tg2.isOn = true;
		tg3.isOn = true;
	}
	public void AddScore(int s){
		if (time >= 0.0f) {
			score += s;
		}
	}

}

