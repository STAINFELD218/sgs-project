﻿using UnityEngine;
using System.Collections;

public class FinalRenderer : MonoBehaviour {
	public Material mat;
	public Camera otherCamera;
	public Shader shader;
	public Shader depthShader;
	RenderTexture depthTexture;
	Camera cam;

	// Use this for initialization
	void Start () {
		//mat = new Material(shader);
		cam = GetComponent<Camera> ();
		cam.depthTextureMode=DepthTextureMode.Depth;
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	void onPreRender() {
		RenderTexture currentRT = RenderTexture.active;
		cam.targetTexture = depthTexture;
		RenderTexture.active = cam.targetTexture;
		cam.RenderWithShader (depthShader, "");
		cam.targetTexture = currentRT;
	}

	void OnRenderImage (RenderTexture source, RenderTexture destination){
		//mat is the material containing your shader
		mat.SetTexture ("_WaterTex", source);
		mat.SetTexture ("_WaterDepthTex", depthTexture);
		mat.SetTexture("_MainColorTex", otherCamera.GetComponent<MainCameraScript> ().getTexture());
		mat.SetTexture("_MainDepthTex", otherCamera.GetComponent<MainCameraScript> ().getDepthTexture());
		//Debug.Log(mat.HasProperty("Sec"));
		Graphics.Blit(source,destination,mat);
	}
}
