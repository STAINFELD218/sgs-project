﻿using UnityEngine;
using System.Collections;

public class DepthRenderer : MonoBehaviour {
	Camera cam;
	public Material mat;
	public RenderTexture depthTexture;
	// Use this for initialization
	void Start () {
		cam = GetComponent<Camera> ();
	}
	
	// Update is called once per frame
	void Update () {
	
	}
	

	void OnRenderImage (RenderTexture source, RenderTexture destination){
		//cam.targetTexture = depthTexture;
		//cam.RenderWithShader (depthShader, null);
		Graphics.Blit(source,depthTexture,mat);
	}
}
