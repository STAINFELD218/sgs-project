﻿using UnityEngine;
using System.Collections;

public class MainCameraScript : MonoBehaviour {
	RenderTexture texture;
	RenderTexture depthTexture;
	public Shader depthShader;
	Camera cam;
	// Use this for initialization
	void Start () {
		cam = GetComponent<Camera> ();

		//RenderTextureFormat format = RenderTextureFormat.ARGB32;

		RenderTexture renderTexture = new RenderTexture(Screen.width, Screen.height, 32);
		renderTexture.Create();
		cam.targetTexture = renderTexture;
		texture = renderTexture;

		cam.depthTextureMode=DepthTextureMode.Depth;

		depthTexture = new RenderTexture(Screen.width, Screen.height, 24, RenderTextureFormat.ARGBFloat);
		depthTexture.Create();

		//camera.SetTargetBuffers (texture, depthTexture);
		//camera.targetTexture = depthTexture;
	}
	
	// Update is called once per frame
	void Update () {
	//	camera.targetTexture = depthTexture;
	//	camera.RenderWithShader (depthShader, "Opaque");
	//	camera.targetTexture = null;
		render ();
	}

	void render() {
		RenderTexture currentRT = RenderTexture.active;
		cam.targetTexture = texture;
		RenderTexture.active = cam.targetTexture;
		cam.Render ();

		cam.targetTexture = depthTexture;
		RenderTexture.active = cam.targetTexture;
		cam.RenderWithShader (depthShader, "");
		RenderTexture.active = currentRT;
	}

	//void onPreRender() {
		//Debug.Log ("fuck");
	//	cam.targetTexture = depthTexture;
	//	cam.RenderWithShader (depthShader, "");
	//	cam.targetTexture = texture;
	//	cam.Render ();
	//}

	public RenderTexture getTexture() {
		return texture;
	}

	public RenderTexture getDepthTexture() {
		return depthTexture;
	}
}
