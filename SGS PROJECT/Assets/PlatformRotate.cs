﻿using UnityEngine;
using System.Collections;

public class PlatformRotate : MonoBehaviour {
	Rigidbody body;

	public float speed;
	// Use this for initialization
	void Start () {
		body = GetComponent<Rigidbody> ();
	}
	
	// Update is called once per frame
	void Update () {
		body.transform.Rotate (new Vector3(0, 0, speed * Time.deltaTime));
	}
}
